LexikJWTAuthenticationBundle Sandbox
=====================================

This is a sample application for experimenting/demonstrating features of the powerful LexikJWTAuthenticationBundle bundle which provides authentication through JWT.

What's inside
--------------

- [Symfony](https://github.com/symfony/symfony) 4.0 (Flex)
- [LexikJWTAuthenticationBundle](https://github.com/lexik/LexikJWTAuthenticationBundle) ~2.4

Get started
------------

Assuming Docker and docker-compose already installed:
```sh
$ docker-compose up -d
```

Install dependencies and libraries:

```
$ composer install
```

Create the database schema:
```sh
$ docker-compose exec web bin/console doctrine:database:create
$ docker-compose exec bin/console doctrine:schema:update --force
```

Usage
------

Register a new user:
```
$ curl -X POST http://localhost:3333/register -d _username=johndoe -d _password=test
-> User johndoe successfully created
```

Get a JWT token:
```
$ curl -X POST -H "Content-Type: application/json" http://localhost:3333/login_check -d '{"username":"johndoe","password":"test"}'
-> { "token": "[TOKEN]" }  
```

Access a secured route:
```
$ curl -H "Authorization: Bearer [TOKEN]" http://localhost:3333/api
-> Logged in as johndoe
```
